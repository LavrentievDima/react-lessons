import React from "react";

const AppHeader = ({toDo, done}) => {
    return(
        <div>
            <h1>TodoList</h1>
            todo: {toDo}, done: {done}
        </div>
    )
}

export default  AppHeader