import React from "react";

import './to-do-list-item.css'

const TodoListItem = ({ label, onDeleted, done, important, toggleImportant, toggleDone }) => {

    let className = 'todo-list-item';
    if(done) {
        className += ' done'
    }
    if(important) {
        className += ' important'
    }

    return (
        <div className={className}>
            <span
                className='todo-list-item'
                onClick={ toggleDone }
            >
                {label}
            </span>
            <button type="button" className="btn btn-outline-danger" onClick={ onDeleted }>
                <i className="fa fa-trash-o"/>
            </button>
            <button type="button" className="btn btn-outline-success" onClick={ toggleImportant }>
                <i className="fa fa-exclamation"/>
            </button>
        </div>
    )
};

export default TodoListItem;