import React, { Component } from "react";

export default class Filters extends Component{

    state= {
        btnList: [
            { id:0, name: 'All', active: true },
            { id:1, name: 'Active', active: false },
            { id:2, name: 'Done', active: false },
        ]
    };

    setActive = ({target}) => {
        const currentFilter = target.dataset.name;
        this.props.filter(currentFilter);
        this.setState( ({btnList}) => {
            return {
                btnList: btnList.map( el => {
                    if(el.name === currentFilter) {
                        return {...el, active: true}
                    }
                    return {...el, active: false}
                })
            }
        })
    }

    render() {
        const items = this.state.btnList.map( el => {
            let cl = 'btn btn-secondary'
            if(el.active) cl += ' active';
            return <button type="button" data-name={el.name} className={cl} key={el.id} onClick={this.setActive}>{el.name}</button>
        });
        return (
            <div className="btn-group" role="group" aria-label="Basic example">
                {items}
            </div>
        )
    }
}