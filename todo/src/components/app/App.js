import React, { Component } from 'react';

import AppHeader from '../app-header'
import SearchPanel from '../search-panel'
import TodoList from '../to-do-list'
import Filters from '../filters'
import AddItem from "../add-item";

export default class App extends Component {
    state = {
        isShowTime: true,
        todoData: [
            {id: 0, label: 'dgdsg', important: true, done: true},
            {id: 1, label: 'adhd', important: false, done: false},
            {id: 2, label: 'fjdfjdhjd', important: false, done: false},
        ],

        str: '',
        filter: 'All',
    };

    maxId = () => {
        return this.state.todoData.reduce((res, el)  => {
            if(res < el.id){
                return el.id
            }
            return res
        }, 0)
    };

    removeItem = id => {
        this.setState( ({ todoData }) => {
            return {
                todoData: todoData.filter( el => el.id !== id)
            }
        })
    };

    addItem = name => {
        this.setState( ({ todoData }) => {
            const newItem = {
                id: this.maxId() + 1,
                label: name,
                important: false
            }
            return {
                todoData: [...todoData, newItem]
            }
        })
    };

    toggleImportant = (id) => {
        this.setState( ({todoData}) => {
            return {
                todoData: todoData.map( el => {
                    if(el.id === id) {
                        return {...el, important: !el.important}
                    }
                    return el;
                })
            }
        })
    };

    toggleDone = (id) => {
        this.setState( ({todoData}) => {
            return {
                todoData: todoData.map( el => {
                    if(el.id === id) {
                        return {...el, done: !el.done}
                    }
                    return el;
                })
            }
        })
    };

    setStr = (str) => {
        this.setState({str})
    }
    search = (items, str = '') => {
        return items.filter( el => el.label.includes(str))
    };

    setFilter = (filter) => {
        this.setState({filter})
    };

    filtered = (items, filterName) =>{
        return items.filter(el => {
            if(filterName === 'Active' && !el.done) {
                return true
            }
            if(filterName === 'Done' && el.done) {
                return true
            }
            if(filterName === 'All') {
                return true
            }
        })
    }

    filteredItem = () => {
        const {todoData, str, filter} = this.state;

        return this.search(this.filtered(todoData, filter), str);
    }

    render() {
        const {isShowTime} = this.state;
        const time = (<span>{(new Date()).toLocaleString()}</span>);

        const doneCount = this.state.todoData.filter( el => el.done).length;
        const todoCounter = this.state.todoData.length - doneCount;

        return (
            <div>
                { isShowTime ? time : null }
                <AppHeader toDo={todoCounter} done={doneCount}/>
                <SearchPanel search={this.setStr}/>
                <Filters filter={this.setFilter}/>
                <AddItem addItem={ this.addItem }/>

                <TodoList
                    deleted={ this.removeItem }
                    toggleImportant={this.toggleImportant}
                    toggleDone={this.toggleDone}
                    todos={this.filteredItem()}
                />
            </div>
        )
    }
}

