import React from "react";

const SearchPanel = ({search}) => {
    const plh = 'search';
    return <input placeholder={plh} onChange={({target}) => search(target.value)}/>;
}

export default SearchPanel;