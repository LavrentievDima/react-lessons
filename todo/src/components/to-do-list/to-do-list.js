import React from "react";

import TodoListItem from '../to-do-list-item'

import './to-do-list.css'

const TodoList = ({ todos, deleted, toggleImportant, toggleDone }) => {

    const elements = todos.map( el => {
        const { id, ...props} = el;
        return (
            <li key={id} className="list-group-item">
                <TodoListItem
                    onDeleted={ () => deleted(id) }
                    toggleImportant={ () => toggleImportant(id)}
                    toggleDone={ () => toggleDone(id)}
                    {...props}
                />
            </li>
        )
    })

    return (
        <ul className='list-group todo-list'>
            {elements}
        </ul>
    );
}

export default TodoList;