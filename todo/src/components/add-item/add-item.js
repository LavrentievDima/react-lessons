import React, { Component } from "react";

export default class AddItem extends Component {
    state = {
        name: '',
    }

    addItem = (e) => {
        e.preventDefault();
        this.props.addItem(this.state.name);
        this.setState({name: ''})
    }

    onChangeInput = (e) => {
        this.setState({name: e.target.value})
    }

    render() {
        return (
            <div>
                <div>{this.state.name} </div>
                <form
                    className="d-flex"
                    onSubmit={this.addItem}
                >
                    <input
                        type='text'
                        className="form-control"
                        onChange={this.onChangeInput}
                        placeholder="la la la"
                        value={this.state.name}
                    />
                    <button type="submit" className="btn btn-outline-primary">
                        <i className="fa fa-plus"/>
                    </button>
                </form>
            </div>
        )
    }
}
